#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

sys.path.append("/home/ghost/Desktop/Projects/OpsdroidSkills/CommonLibs")

from opsdroid.skill import Skill
from opsdroid.matchers import match_regex
from request_ext import requester
from security_ext import check_security_policy

import json
import os

CONFIG_BASE = "/home/ghost/.config/opsdroid"
APP_CONFIG = json.loads(open(os.path.join(CONFIG_BASE, "skill-couchbase/config.json"), "r").read())


class Couchbase(Skill):
    def __init__(self, opsdroid, config):
        super().__init__(opsdroid, config)

    @staticmethod
    def is_bucket_exists(bucket, base_url, credential):
        url = base_url + "/pools/default/buckets/" + str(bucket)
        status, (r_code, _, _, _) = requester(url=url, auth=credential)
        if status and r_code == 200:
            return True

    @match_regex("couchbase\s+list\s+buckets\s+env\s+([a-z]+)")
    async def list_all_buckets(self, message):

        # Define environment variable
        environment = message.regex.group(1).strip()

        # Check security policy
        if not check_security_policy(security_policy="public", message=message, environment=environment):
            await message.respond("@{0}*,You don't have a permission to do this operation!!!*".format(message.user))
            return

        # Check environment config is available
        if environment not in APP_CONFIG.keys():
            await message.respond("@{0}*, '{1}' named environment does not exists..*".format(message.user, environment))
            return

        await message.respond("@{0}, I'm searching couchbase bucket for *{1}* named environment.".format(message.user, environment))
        bucket_list = list()
        env_config = APP_CONFIG[environment]
        url = env_config["url"] + "/pools/default/buckets"
        credential = None if not env_config["auth"] else tuple(env_config["auth_payload"])
        try:
            status, (r_code, response, _, _) = requester(url=url, auth=credential, r_type="json")
            if status and r_code == 200:
                for bucket in response:
                    bucket_list.append(bucket["name"])
                await message.respond("@{0}, I've got *{1}* bucket for *{2}* named environment.\n{3}".format(message.user, len(bucket_list), environment, "\n".join(["*{0}*".format(i) for i in bucket_list])))
            else:
                raise ValueError(response)
        except Exception as e:
            await message.respond("@{0}, Got exception when trying to execute operation. Exception: *{1}*".format(message.user, e))

    @match_regex("couchbase\s+info\s+bucket\s+env\s+([a-z]+)\s+bucket\s+name\s+([a-zA-Z0-9\-_]+)")
    async def get_information_about_bucket(self, message):

        # Define environment variable
        environment = message.regex.group(1).strip()
        # Define list of bucket
        bucket_name = message.regex.group(2).strip()

        # Check security policy
        if not check_security_policy(security_policy="public", message=message, environment=environment):
            await message.respond("@{0}*,You don't have a permission to do this operation!!!*".format(message.user))
            return

        # Check environment config is available
        if environment not in APP_CONFIG.keys():
            await message.respond("@{0}*, '{1}' named environment does not exists..*".format(message.user, environment))
            return

        await message.respond("@{0}, I'm getting info the *'{1}'* named bucket on *{2}* named environment.".format(message.user, bucket_name, environment))
        env_config = APP_CONFIG[environment]
        url = env_config["url"] + "/pools/default/buckets/{0}".format(bucket_name)
        credential = None if not env_config["auth"] else tuple(env_config["auth_payload"])
        if not self.is_bucket_exists(bucket=bucket_name, base_url=env_config["url"], credential=credential):
            await message.respond("@{0}*, '{1}' named bucket does not found.*".format(message.user, bucket_name))
            return
        try:
            status, (r_code, response, _, _) = requester(url=url, auth=credential, r_type="json")
            if status and r_code == 200:
                bucket_stats = response["basicStats"]
                bucket_stats["bucketType"] = response["bucketType"]
                await message.respond("@{0}, Information about *'{1}'* is below.\n```{2}```".format(message.user, bucket_name, json.dumps(bucket_stats, indent=4, sort_keys=True)))
            else:
                raise ValueError(response)
        except Exception as e:
            await message.respond("@{0}, Got exception when trying to execute operation. Exception: *{1}*".format(message.user, e))

    @match_regex("couchbase\s+bucket\s+flush\s+env\s+([a-z]+)\s+bucket\s+name\s+([a-zA-Z0-9,\-_]+)")
    async def flush_list_of_buckets(self, message):

        # Define environment variable
        environment = message.regex.group(1).strip()
        # Define list of bucket
        bucket_list = [i.strip() for i in message.regex.group(2).split(",")]

        # Check security policy
        if not check_security_policy(security_policy="private", message=message, environment=environment):
            await message.respond("@{0}*,You don't have a permission to do this operation!!!*".format(message.user))
            return

        # Check environment config is available
        if environment not in APP_CONFIG.keys():
            await message.respond("@{0}*, '{1}' named environment does not exists..*".format(message.user, environment))
            return

        await message.respond("@{0}, I'm flushing the buckets on this list for *{1}* named environment.".format(message.user, environment))
        env_config = APP_CONFIG[environment]
        url = env_config["url"] + "/pools/default/buckets/{0}/controller/doFlush"
        credential = None if not env_config["auth"] else tuple(env_config["auth_payload"])
        for bucket in bucket_list:
            if not self.is_bucket_exists(bucket=bucket, base_url=env_config["url"], credential=credential):
                await message.respond("@{0}*, '{1}' named bucket does not found.*".format(message.user, bucket))
                continue
            try:
                status, (r_code, response, _, _) = requester(url=url.format(bucket), auth=credential, method="POST")
                if status and r_code == 200:
                    await message.respond("@{0}, *'{1}'* named bucket is flushed.".format(message.user, bucket))
                else:
                    raise ValueError(response)
            except Exception as e:
                await message.respond("@{0}, Got exception when trying to execute operation. Exception: *{1}*".format(message.user, e))

    @match_regex("couchbase\s+search\s+item\s+key\s+'(.*)'\s+env\s+([a-z]+)\s+bucket\s+name\s+([a-zA-Z0-9\-_]+)")
    async def search_key_in_bucket(self, message):

        # Define search key
        target_key = message.regex.group(1).strip()
        # Define environment variable
        environment = message.regex.group(2).strip()
        # Define list of bucket
        bucket_name = message.regex.group(3).strip()

        # Check security policy
        if not check_security_policy(security_policy="public", message=message, environment=environment):
            await message.respond("@{0}*,You don't have a permission to do this operation!!!*".format(message.user))
            return

        # Check environment config is available
        if environment not in APP_CONFIG.keys():
            await message.respond("@{0}*, '{1}' named environment does not exists..*".format(message.user, environment))
            return

        await message.respond("@{0}, I'm searching *'{1}'* named key in the *'{2}'* named bucket on *{3}* named environment.".format(message.user, target_key, bucket_name, environment))
        env_config = APP_CONFIG[environment]
        url = env_config["url"] + "/pools/default/buckets/{0}/docs?include_docs=true&limit=3&skip=0&startkey=\"{1}\""
        credential = None if not env_config["auth"] else tuple(env_config["auth_payload"])
        try:
            status, (r_code, response, _, _) = requester(url=url.format(bucket_name, target_key), auth=credential, r_type="json")
            if status and r_code == 200:
                await message.respond("@{0}, Information about *'{1}'* is below.\n```{2}```".format(message.user, target_key, json.dumps(response, indent=4, sort_keys=True)))
            else:
                raise ValueError(response)
        except Exception as e:
            await message.respond("@{0}, Got exception when trying to execute operation. Exception: *{1}*".format(message.user, e))

    @match_regex("couchbase\s+list\s+views\s+env\s+([a-z]+)")
    async def list_all_views(self, message):

        # Define environment variable
        environment = message.regex.group(1).strip()

        # Check security policy
        if not check_security_policy(security_policy="public", message=message, environment=environment):
            await message.respond("@{0}*,You don't have a permission to do this operation!!!*".format(message.user))
            return

        # Check environment config is available
        if environment not in APP_CONFIG.keys():
            await message.respond("@{0}*, '{1}' named environment does not exists..*".format(message.user, environment))
            return

        await message.respond("@{0}, I'm searching couchbase views for *{1}* named environment.".format(message.user, environment))
        return_data = dict()
        env_config = APP_CONFIG[environment]
        url = env_config["url"] + "/pools/default/buckets"
        credential = None if not env_config["auth"] else tuple(env_config["auth_payload"])
        try:
            status, (r_code, response, _, _) = requester(url=url, auth=credential, r_type="json")
            if status and r_code == 200:
                for bucket in response:
                    sub_url = env_config["url"] + "/pools/default/buckets/{0}/ddocs".format(bucket["name"])
                    s_status, (s_r_code, s_response, _, _) = requester(url=sub_url, auth=credential, r_type="json")
                    if s_status and s_r_code == 200:
                        if len(s_response["rows"]) > 0:
                            return_data[bucket["name"]] = list()
                            for i in s_response["rows"]:
                                tmp = dict()
                                tmp["document_name"] = i["doc"]["meta"]["id"]
                                for e, s in i["doc"]["json"].items():
                                    if e in ["views", "spatial"]:
                                        tmp["view_type"] = e if e == "spatial" else "view"
                                        tmp["view_names"] = [k for k, _ in s.items()]
                                return_data[bucket["name"]].append(tmp)
                    else:
                        raise ValueError(s_response)
                if len(return_data) > 0:
                    await message.respond("@{0}, Information about views is below.\n```{1}```".format(message.user, json.dumps(return_data, indent=4, sort_keys=True)))
                else:
                    await message.respond("@{0}, no views found.".format(message.user))
            else:
                raise ValueError(response)
        except Exception as e:
            await message.respond("@{0}, Got exception when trying to execute operation. Exception: *{1}*".format(message.user, e))

    @match_regex("couchbase\s+run\s+view\s+document\s+name\s+'(.*)'\s+view\s+name\s+'(.*)'\s+view\s+type\s+(spatial|view)\s+env\s+([a-z]+)\s+bucket\s+name\s+([a-zA-Z0-9\-_]+)")
    async def run_view(self, message):

        # Define document name
        document_name = message.regex.group(1).strip()
        # Define view name
        view_name = message.regex.group(2).strip()
        # Define view type
        view_type = message.regex.group(3).strip()
        # Define environment variable
        environment = message.regex.group(4).strip()
        # Define bucket name
        bucket_name = message.regex.group(5).strip()

        # Check security policy
        if not check_security_policy(security_policy="public", message=message, environment=environment):
            await message.respond("@{0}*,You don't have a permission to do this operation!!!*".format(message.user))
            return

        # Check environment config is available
        if environment not in APP_CONFIG.keys():
            await message.respond("@{0}*, '{1}' named environment does not exists..*".format(message.user, environment))
            return

        await message.respond("@{0}, I'm executing the view of the *{1}* named bucket on *{2}* named environment.".format(message.user, bucket_name, environment))
        env_config = APP_CONFIG[environment]
        url = env_config["url"] + "/couchBase/{bucket_name}/{document_name}/_{view_type}/{view_name}?connection_timeout=60000&limit=6&skip=0&stale=false".format(
            bucket_name=bucket_name, document_name=document_name, view_type=view_type, view_name=view_name
        )
        credential = None if not env_config["auth"] else tuple(env_config["auth_payload"])
        try:
            status, (r_code, response, _, _) = requester(url=url, auth=credential, r_type="json")
            if status and r_code == 200:
                await message.respond("@{0}, Results about *'{1}/{2}'* is below.\n```{3}```".format(message.user, document_name, view_name, json.dumps(response, indent=4, sort_keys=True)))
            else:
                raise ValueError(response)
        except Exception as e:
            await message.respond("@{0}, Got exception when trying to execute operation. Exception: *{1}*".format(message.user, e))

    @match_regex("couchbase\s+list\s+index\s+bucket\s+name\s+([a-zA-Z0-9\-_]+)\s+env\s+([a-z]+)")
    async def list_all_index_in_bucket(self, message):

        # Define bucket name
        bucket_name = message.regex.group(1).strip()

        # Define environment variable
        environment = message.regex.group(2).strip()

        # Check security policy
        if not check_security_policy(security_policy="public", message=message, environment=environment):
            await message.respond("@{0}*,You don't have a permission to do this operation!!!*".format(message.user))
            return

        # Check environment config is available
        if environment not in APP_CONFIG.keys():
            await message.respond("@{0}*, '{1}' named environment does not exists..*".format(message.user, environment))
            return

        await message.respond("@{0}, I'm searching index on '{1}' named couchbase bucket for *{2}* named environment.".format(message.user, bucket_name, environment))
        index_list = list()
        env_config = APP_CONFIG[environment]
        url = env_config["url"] + "/indexStatus"
        credential = None if not env_config["auth"] else tuple(env_config["auth_payload"])

        # Check bucket is exists
        if not self.is_bucket_exists(bucket=bucket_name, base_url=env_config["url"], credential=credential):
            await message.respond("@{0}*, '{1}' named bucket does not found.*".format(message.user, bucket_name))
            return
        try:
            status, (r_code, response, _, _) = requester(url=url, auth=credential, r_type="json")
            if status and r_code == 200:
                for index in response["indexes"]:
                    if index["bucket"] == bucket_name:
                        index_list.append({
                            "IndexName": index["index"],
                            "StorageMode": index["storageMode"],
                            "Progress": index["progress"],
                            "Definition": index["definition"],
                            "Status": index["status"]
                        })
                await message.respond("@{0}, I found *{1}* index belongs to the *'{2}'* named bucket. I listed them below.\n```{3}```".format(message.user, len(index_list), bucket_name, json.dumps(index_list, indent=4, sort_keys=True)))
            else:
                raise ValueError(response)
        except Exception as e:
            await message.respond("@{0}, Got exception when trying to execute operation. Exception: *{1}*".format(message.user, e))
