#!/usr/bin/python
# -*- coding: utf-8 -*-

from socket import socket
from subprocess import Popen, PIPE


def connection_test(host, port, application_name):
    s = socket()
    try:
        s.connect((host, port))
        return True
    except Exception as e:
        return "{0} host is unreachable.IP: {1}, Port: {2}, Exception: {3}".format(application_name, host, port, e)
    finally:
        s.close()


def run_local_script(script):
    lp = Popen([script], stderr=PIPE, stdout=PIPE, shell=True)
    output = lp.communicate()
    return lp.returncode, output
