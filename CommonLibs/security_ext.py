#!/usr/bin/python
# -*- coding: utf-8 -*-


policy_table = {
    "private": {
        "dev": {
            "users": [
                "yigit.basalma"
            ]
        }
    }
}


def check_security_policy(security_policy, message, environment):
    if security_policy == "private":
        if message.user not in policy_table[security_policy][environment]["users"]:
            return False
    return True
