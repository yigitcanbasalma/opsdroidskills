#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests


def requester(url, headers=None, data=None, auth=None, r_type="text", method="GET", timeout=10, response_headers=False, verify=False):
    try:
        avail_methods = {
            "GET": requests.get,
            "POST": requests.post,
            "PUT": requests.put,
            "DELETE": requests.delete
        }
        if headers is None:
            headers = {
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0 (Opsdroid Chat Bot)"
            }
        data = data if data is not None else dict()
        r = avail_methods[method](url, headers=headers, verify=verify, data=data, auth=auth, timeout=timeout)
        r_headers = ["{0}: {1}".format(k, v) for k, v in r.headers.iteritems()] if response_headers else []
        if r_type == "json":
            return True, (r.status_code, r.json(), r_headers, url)
        return True, (r.status_code, r.text, r_headers, url)
    except Exception as e:
        return False, (0, e, [], url)
